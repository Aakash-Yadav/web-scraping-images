from requests import get
from bs4 import BeautifulSoup

#Download
def download_image(url):
	"Get Binary Data From Url"
	return get(url).content

#Name
def extract_name(url):
	"Convert url into list"
	name = url.split("/")
	return str(name[-1])

#Html_content
def html_doc(n):
    return get(n).text

#Bs4 data 
def Bs_4(data):
    text_data = html_doc(data)
    return BeautifulSoup(text_data,"lxml")

def image_save(n,url):
    with open(n,'wb')as f:
        f.write(download_image(url))   


#Get url image    
def fetch_data_image(url):
    Soup = Bs_4(url)
    for image in (Soup.find_all("img" ,{'class':'wpimg'})):
        # print(image['src'])
        image_save(extract_name(image['src']),'https://wallpapercave.com/'+image['src'])
        print('Save')

if __name__=="__main__":
    fetch_data_image("https://wallpapercave.com/anime-4k-wallpapers")